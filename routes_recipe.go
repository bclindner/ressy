package main

import (
	"fmt"
	"net/http"
	"github.com/julienschmidt/httprouter"
	"strconv"
	"encoding/json"
	"git.local/bclindner/ressy/models"
	"git.local/bclindner/ressy/util"
	"time"
)

const (
	MAX_PAGINATION_LIMIT = 20
	MAX_FILESIZE int64 = 10000000 // 10mb
)

var recipeAcceptableTypes = []string{"application/json", "text/html"}
var htmlOnlyAcceptableType = []string{"text/html"}

// Set up all the routes in this file.
func (s *Server) declareRecipeRoutes() {
	// set up routes
	// /recipes/
	// (redirect /recipes to /recipes/)
	s.Route("GET", "/recipes", http.RedirectHandler("/recipes/", 301))
	s.RouteFunc("GET", "/recipes/",
		s.ListRecipesRoute,
		s.ContentNegotiationMiddleware(recipeAcceptableTypes),
	)
	// /recipes/:id
	s.RouteFunc("GET", "/recipes/:id",
			s.ShowRecipeRoute,
			s.ContentNegotiationMiddleware(recipeAcceptableTypes),
	)
	s.RouteFunc("POST", "/recipes/", s.PostRecipeRoute)
	s.RouteFunc("PATCH", "/recipes/:id",
		s.PatchRecipeRoute,
		s.ContentNegotiationMiddleware(recipeAcceptableTypes),
	)
	s.RouteFunc("DELETE", "/recipes/:id", s.DeleteRecipeRoute)
	// /recipes/:id/edit
	s.RouteFunc("GET", "/recipes/:id/edit",
		s.GetEditRecipeRoute,
		s.ContentNegotiationMiddleware(htmlOnlyAcceptableType),
	)
	s.RouteFunc("POST", "/recipes/:id/edit",
		s.PostEditorRoute,
	)
	// /new-recipe
	s.RouteFunc("GET", "/new-recipe",
		s.GetNewRecipeRoute,
		s.ContentNegotiationMiddleware(htmlOnlyAcceptableType),
	)
	s.RouteFunc("POST", "/new-recipe",
		s.PostEditorRoute,
	)
}

// Serve a recipe object.
// Requires the ContentNegotiationMiddleware.
func (s *Server) serveRecipe(w http.ResponseWriter, r *http.Request, recipe *models.Recipe) {
	// get recipe content type
	// if this is nil then something is seriously seriously wrong - like, panic wrong.
	ctype, _ := r.Context().Value("content-type").(util.ContentType)
	// serve it
	switch ctype.Mime {
		case "application/json":
			s.serveJSON(w, recipe)
		case "text/html":
			s.serveTemplate(w, TemplateData{
				template: "recipe",
				Title: recipe.Name,
				Data: recipe,
			})
	}
}

func (s *Server) serveRecipes(
	w http.ResponseWriter,
	r *http.Request,
	recipes []*models.Recipe,
	paginator *util.Paginator) {
	// get recipe content type
	// if this is nil then something is seriously seriously wrong - like, panic wrong.
	ctype, _ := r.Context().Value("content-type").(util.ContentType)
	// serve it
	switch ctype.Mime {
		case "application/json":
			s.serveJSON(w, recipes)
		case "text/html":
			s.serveTemplate(w, TemplateData{
				template: "recipes",
				Title: "Recipes",
				Data: recipes,
				Query: r.URL.Query(),
				Paginator: paginator,
			})
	}
}

// Show a recipe by its ID.
// Routed to "/recipes/:id".
func (s *Server) ShowRecipeRoute(w http.ResponseWriter, r *http.Request) {
	ps := httprouter.ParamsFromContext(r.Context())
	// parse id and get recipe
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	recipe, err := s.db.GetRecipes().ByID(id).Get()
	if err != nil {
		s.handleError(w, http.StatusNotFound, err)
		return
	}
	s.serveRecipe(w, r, recipe)
}

// List recipes.
// Routed to "/recipes/".
func (s *Server) ListRecipesRoute(w http.ResponseWriter, r *http.Request) {
	qp := r.URL.Query()
	var limit, since, offset int
	// parse limit
	limitStr := qp.Get("limit")
	if len(limitStr) == 0 {
		limit = MAX_PAGINATION_LIMIT
	} else {
		var err error
		limit, err = strconv.Atoi(limitStr)
		if err != nil {
			s.handleError(w, http.StatusBadRequest, fmt.Errorf("could not convert limit: %w", err))
			return
		}
		// clamp pagination limit
		if limit > MAX_PAGINATION_LIMIT || limit < 1 {
			limit = MAX_PAGINATION_LIMIT
		}
	}
	// parse since
	sinceStr := qp.Get("since")
	if len(sinceStr) == 0 {
		since = 0
	} else {
		var err error
		since, err = strconv.Atoi(sinceStr)
		if err != nil {
			s.handleError(w, http.StatusBadRequest, fmt.Errorf("could not convert since: %w", err))
			return
		}
		if since < 0 {
			since = 0
		}
	}
	// parse offset
	offsetStr := qp.Get("offset")
	if len(offsetStr) == 0 {
		offset = 0
	} else {
		since =- 0
		var err error
		offset, err = strconv.Atoi(offsetStr)
		if err != nil {
			s.handleError(w, http.StatusBadRequest, fmt.Errorf("could not convert offset: %w", err))
			return
		}
		if offset < 0 {
			offset = 0
		}
		// overwrite since if offset exists - this takes precedence
		if offset != 0 {
			since = 0
		}
	}
	// start recipe query
	query := s.db.GetRecipes()
	// parse other fields to query
	if param := qp.Get("name"); param != "" {
		query.ByName(param)
	}
	if param := qp.Get("description"); param != "" {
		query.ByDescription(param)
	}
	if param := qp.Get("yields"); param != "" {
		query.ByYields(param)
	}
	if param := qp.Get("ingredient"); param != "" {
		query.ByIngredient(param)
	}
	if param := qp.Get("instruction"); param != "" {
		query.ByInstruction(param)
	}
	if param := qp.Get("query"); param != "" {
		query.ByQuery(param)
	}
	// TODO prep time qp, cook time qp

	// get count and generate paginator
	count, err := query.GetCount()
	if err != nil {
		s.handleError(w, 500, err)
		return
	}
	paginator := &util.Paginator{
		Offset: offset,
		Limit: limit,
		Count: count,
	}
	recipes, err := query.
		Since(since).
		Limit(limit).
		Offset(offset).
		GetAll()
	if err != nil {
		s.handleError(w, 500, err)
		return
	}
	s.serveRecipes(w, r, recipes, paginator)
}

// Insert a recipe.
// Routed to "/recipes/".
func (s *Server) PostRecipeRoute(w http.ResponseWriter, r *http.Request) {
	// parse the recipe into a Recipe
	var recipe models.Recipe
	err := json.NewDecoder(r.Body).Decode(&recipe)
	if err != nil {
		s.handleError(w, 500, err)
		return
	}
	id, err := s.db.InsertRecipe(recipe)
	http.Redirect(w, r, strconv.FormatInt(id, 10), 201)
}

// Patch a recipe.
// Routed to "/recipes/:id".
func (s *Server) PatchRecipeRoute(w http.ResponseWriter, r *http.Request) {
	ps := httprouter.ParamsFromContext(r.Context())
	// parse id and get recipe
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	recipe, err := s.db.GetRecipes().ByID(id).Get()
	if err != nil {
		s.handleError(w, http.StatusNotFound, err)
		return
	}
	// unmarshal over the recipe
	err = json.NewDecoder(r.Body).Decode(&recipe)
	if err != nil {
		s.handleError(w, http.StatusInternalServerError, err)
		return
	}
	// update it in the db
	err = s.db.UpdateRecipe(*recipe)
	if err != nil {
		s.handleError(w, http.StatusInternalServerError, err)
		return
	}
	// serve it
	s.serveRecipe(w, r, recipe)
}

func (s *Server) DeleteRecipeRoute(w http.ResponseWriter, r *http.Request) {
	ps := httprouter.ParamsFromContext(r.Context())
	// parse id and get recipe
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	err = s.db.DeleteRecipe(id)
	if err != nil {
		s.handleError(w, http.StatusInternalServerError, err)
		return
	}
}

func (s *Server) GetEditRecipeRoute(w http.ResponseWriter, r *http.Request) {
	// get the ID
	ps := httprouter.ParamsFromContext(r.Context())
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	// get the recipe by that ID
	recipe, err := s.db.GetRecipes().ByID(id).Get()
	if err != nil {
		s.handleError(w, http.StatusNotFound, err)
	}
	// serve the edit page for the recipe
	s.serveTemplate(w, TemplateData{
		template: "recipe-edit",
		Title: "Edit Recipe",
		Data: recipe,
	})
}

func (s *Server) GetNewRecipeRoute(w http.ResponseWriter, r *http.Request) {
	// serve an empty edit page
	s.serveTemplate(w, TemplateData{
		template: "recipe-edit",
		Title: "Edit Recipe",
		Data: nil,
	})
}

// Post a recipe from the editor form to the DB.
// This handles BOTH the POST /recipes/:id/edit and the POST /new-recipe
// routes. If sent without an "id" httprouter param, then it assumes insert.
func (s *Server) PostEditorRoute(w http.ResponseWriter, r *http.Request) {
	ps := httprouter.ParamsFromContext(r.Context())
	// pre-parse sanity check: if it's over MAX_FILESIZE don't bother reading
	if r.ContentLength > MAX_FILESIZE {
		s.handleError(w, http.StatusRequestEntityTooLarge,
			fmt.Errorf("Oversize request rejected (%d bytes)", r.ContentLength))
		return
	}
	// parse the form out into a new Recipe object
	err := r.ParseMultipartForm(MAX_FILESIZE)
	if err != nil {
		// we already handled the 413 use case, so this is *probably* a bad body
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	recipe := models.Recipe{}
	// check if ID is set in the URL: if so, this is an update
	idstr := ps.ByName("id")
	s.logger.Info("%s", idstr)
	if idstr != "" {
		id, err := strconv.Atoi(ps.ByName("id"))
		if err != nil {
			s.handleError(w, http.StatusBadRequest, err)
			return
		}
		recipe.ID = int64(id)
	}
	// name
	recipe.Name = r.Form.Get("name")
	if recipe.Name == "" {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	// TODO image importing
	// below is a placeholder
	recipe.Image = r.Form.Get("TODO-image")
	// description
	recipe.Description = r.Form.Get("description")
	if recipe.Description == "" {
		s.handleError(w, http.StatusBadRequest, err)
		return
	}
	// prep time
	prepTime := r.Form.Get("preptime")
	if prepTime != "" {
		duration, err := time.ParseDuration(prepTime)
		if err != nil {
			s.handleError(w, http.StatusBadRequest, err)
			return
		}
		recipe.PrepTime = duration
	}
	// cook time
	cookTime := r.Form.Get("cooktime")
	if cookTime != "" {
		duration, err := time.ParseDuration(cookTime)
		if err != nil {
			s.handleError(w, http.StatusBadRequest, err)
			return
		}
		recipe.CookTime = duration
	}
	// yields
	recipe.Yields = r.Form.Get("yields")
	// ingredients
	ingredientIdx := 0
	for {
		ingredient := r.Form.Get("ingredient-" + strconv.Itoa(ingredientIdx))
		if ingredient == "" {
			break
		}
		recipe.Ingredients = append(recipe.Ingredients, models.Ingredient(ingredient))
		ingredientIdx += 1
	}
	// instructions
	instructionIdx := 0
	for {
		instruction := r.Form.Get("instruction-" + strconv.Itoa(instructionIdx))
		if instruction == "" {
			break
		}
		recipe.Instructions = append(recipe.Instructions, models.Instruction(instruction))
		instructionIdx += 1
	}
	// if the id string is empty, then we're inserting. otherwise, we're updating
	if idstr == "" {
		id, err := s.db.InsertRecipe(recipe)
		if err != nil {
			s.handleError(w, http.StatusInternalServerError, err)
			return
		}
		idstr = strconv.FormatInt(id, 10)
	} else {
		err = s.db.UpdateRecipe(recipe)
		if err != nil {
			s.handleError(w, http.StatusInternalServerError, err)
			return
		}
	}
	// redirect to the updated recipe
	http.Redirect(w, r, "/recipes/" + ps.ByName("id"), http.StatusSeeOther)
}
