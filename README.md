# Ressy

Ressy (name not final) is an experimental recipe web app I designed for my own
in-home use. It is written in Go, with the app portions written in standard Go
HTML templates, CSS, and a tiny amount of JavaScript required for certain parts
of the app.

## Features

Ressy is a primitive application but boasts some important features that make it
a helpful utility for any self-hoster:

* A fully-featured JSON REST API, with the ability to create, read, update, and
	delete recipes programmatically, allowing integration with external clients or
	other apps on the network
* A responsive, mobile-first Web application, perfect for checking recipes
	on your phone or a tablet while in the kitchen
* A flexible search engine allowing searching (nearly) every part of a recipe,
	including ingredients and instructions

## Dependencies

Ressy uses as few dependencies as is reasonably possible. While I do think code
quality has suffered as a result, I have my reasons for doing it this way; this
could very easily be re-implemented in a framework, and it would very likely
behave better, but there is something alluring and educational about working as
close to the language as possible.

As of right now, there are only two dependencies:

* `github.com/mattn/go-sqlite3`, for data persistence
* `github.com/julienschmidt/httprouter`, for fast routing with a much easier
	method for extracting parameters.

## Developing

Currently there are a few caveats to development:

* Currently the template packing system is a bit fragile. **Do not use
	backticks in templates -** they will break the current (very primitive)
	template packing system.
* Run `go generate` before each commit, and after each change to the template
	files. This regenerates `templates.go`, which is what is ultimately parsed
	out, so your template changes won't be visible until you do this. Doing so
	after each commit ensures users starting the app via `go install` can properly
	launch the application.

Other than that, this is a fairly standard Go application - you can launch with
`go run .`, and build with `go build`. Currently the `assets` folder is not
packed and must be present when launching the application - otherwise the app
will not start, or will be missing files (i.e. the CSS, images, and scripts)
necessary for the application to run.
