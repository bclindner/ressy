function addEntry(name, listID, templateID) {
	// get the list to append to
	const list = document.getElementById(listID)
	// get index to set name with
	const index = list.childElementCount;
	// instantiate the element to add
	const input = document.getElementById(templateID).content.cloneNode(true);
	const li = document.createElement("li");
	// set name
	input.children[0].setAttribute("name", name + "-" + index)
	// add to document
	li.appendChild(input);
	list.appendChild(li);
}

const addIngredient = () => addEntry("ingredient", "ingredient-list", "template-ingredient")
const addInstruction = () => addEntry("instruction", "instruction-list", "template-instruction")
