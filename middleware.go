package main

import (
	"net/http"
	"git.local/bclindner/ressy/util"
	"context"
	"time"
)

type Middleware func(next http.Handler) http.Handler

// Apply middleware to a handler.
func ChainMiddleware(handler http.Handler, middleware ...Middleware) http.Handler {
	for _, m := range middleware {
		handler = m(handler)
	}
	return handler
}

// Middleware that logs each request.
func (s *Server) LogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		st := time.Now()
		next.ServeHTTP(w, r)
		et := time.Now()
		time := et.Sub(st)
		s.logger.InfoOutput(1,"%s %s %s (served in %s)", r.Method, r.URL.Path, r.RemoteAddr, time)
	})
}

// Higher-order middleware that performs HTTP content negotiation by reading
// the request Accept header.
// Adds the result to context with the key "content-type".
func (s *Server) ContentNegotiationMiddleware(acceptableTypes []string) Middleware {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				// get content type
				ctype, err := util.ContentNegotiate(r, acceptableTypes)
				if err != nil {
					// if it's failed because there's no acceptable format we need to
					// change the status code to reflect that
					var status int
					if _, ok := err.(util.NoAcceptableFormatError); ok {
						status = http.StatusNotAcceptable
					} else {
						status = http.StatusBadRequest
					}
					s.handleError(w, status, err)
				}
				ctx := context.WithValue(r.Context(), "content-type", ctype)
				next.ServeHTTP(w, r.WithContext(ctx))
			})
		}
}
