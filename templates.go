
package main

// auto-generated template pack file

const TEMPLATES =`{{ define "footer" }}
<footer>
  <div class="wrapper">
    <p class="textshadow">&copy; Brian Lindner, 2020.</p>
  </div>
</footer>
{{ end }}
{{ define "head" }}
<title>Ressy - {{ .Title }}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/assets/style.css" />
{{ end }}
{{ define "header" }}
<header>
  <div class="wrapper">
    <h1>
      <a href="/" class="nounderline textshadow">
        Ressy
      </a>
    </h1>
    <nav class="textshadow">
      <ul>
        <li>
          <a href="/">Home</a>
        </li>
        <li>
          <a href="/recipes">Recipes</a>
        </li>
        <li>
          <a href="/new-recipe">New</a>
        </li>
      </ul>
    </nav>
  </div>
</header>
{{ end }}
{{ define "home" }}
<!doctype html>
<html>
	<head>
		{{ template "head" . }}
	</head>
	<body>
		<main class="hero">
			<div class="wrapper">
				<form class="searchform" action="/recipes/" method="GET">
					<h1 class="textshadow">Ressy</h1>
					<p class="textshadow-small">
						a recipe management app by <a class="brian" href="https://bclindner.com">Brian!</a>
					</p>
					<input class="searchbar" name="query" placeholder="Search for recipes..." value=""/>
					<input type="submit" value="Search" />
				</form>
				<div class="card-deck">
					{{ range $recipe := .Data }}
						{{ template "recipe-card" $recipe }}
					{{ end }}
				</div>
			</div>
		</main>
		{{ template "footer" . }}
	</body>
</html>
{{ end }}
{{ define "paginator" }}
<div class="paginator">
	{{ if .Paginator.Prev }}
	<a href="{{ .Paginator.PrevURL }}" class="button paginator-button textshadow nounderline">
	&lt;
	</a>
	{{ end }}
	Page {{ .Paginator.CurrentPageNumber }} of {{ .Paginator.MaxPageNumber }}
	{{ if .Paginator.Next }}
	<a href="{{ .Paginator.NextURL }}" class="button paginator-button textshadow nounderline">
	&gt;
	</a>
	{{ end }}
</div>
{{ end }}
{{ define "recipe-card" }}
<div class="card">
  <a href="{{ .ID }}" class="nounderline">
    <img class="responsive" src="{{ .Image }}" />
    <h3>{{ .Name }}</h3>
  </a>
  <p class="card-body">{{ .Description }}</p>
  <p>Prep time {{ .PrepTime }} &mdash; Cook time {{ .CookTime }}</p>
</div>
{{ end }}
{{ define "partials/recipe-edit" }}
<template id="template-ingredient">
	<input class="edit edit-entry" />
</template>
<template id="template-instruction">
	<textarea rows="2" class="edit edit-entry"></textarea>
</template>
<form method="POST" enctype="multipart/form-data">
	<article id="recipe-editor" class="recipe recipe-edit">
			<section id="meta">
				<div class="recipe-header">
					<input
						name="name"
						placeholder="Input a name..."
						value="{{if .Name}}{{.Name}}{{end}}"
						class="edit edit-name"
					/>
					<input type="submit" value="Save" class="button textshadow">
				</div>
				<div class="recipe-image">
					<img class="responsive" src="{{ .Image }}" alt="Picture of {{ .Name }}"/>
					<input name="image" type="file" class="edit-file">
					<input name="TODO-image" type="hidden" value="{{.Image}}">
				</div>
				<p>
					<textarea
						rows="4"
						name="description"
						placeholder="Input a description..."
						class="edit edit-desc"
					>{{if .Description}}{{.Description}}{{end}}</textarea>
				</p>
				<p>
					<b>Prep time:</b>
					<input
						name="preptime"
						placeholder="(optional)"
						value="{{if .PrepTime}}{{.PrepTime}}{{end}}"
						class="edit edit-field"
					/>
				</p>
				<p>
					<b>Cook time:</b>
					<input
						name="cooktime"
						placeholder="(optional)"
						value="{{if .CookTime}}{{.CookTime}}{{end}}"
						class="edit edit-field"
					/>
				</p>
				<p>
					Serves
					<input
						name="yields"
						placeholder="(optional)"
						value="{{if .Yields}}{{.Yields}}{{end}}"
						class="edit edit-field"
					/>
				</p>
			</section>
			<section id="ingredients">
				<h2>Ingredients</h2>
				<ul id="ingredient-list">
					{{ range $idx, $ingredient := .Ingredients }}
					<li>
						<input
							name="ingredient-{{ $idx }}"
							value="{{ $ingredient }}"
							class="edit edit-entry"
						/>
					</li>
					{{ end }}
				</ul>
				<button class="button" onclick="addIngredient()" type="button">New</button>
			</section>
			<section id="instructions">
				<h2>Instructions</h2>
				<ol id="instruction-list">
					{{ range $idx, $instruction := .Instructions }}
					<li>
						<textarea
							rows="3"
							name="instruction-{{ $idx }}"
							value="{{ $instruction }}"
							class="edit edit-entry"
						>{{ $instruction }}</textarea>
					</li>
					{{ end }}
				</ol>
				<button class="button" onclick="addInstruction()" type="button">New</button>
			</section>
	</article>
</form>
{{ end }}


{{ define "recipe-edit" }}
<!doctype html>
<html>
	<head>
		{{ template "head" . }}
		<script src="/assets/scripts/recipe-editor.js"></script>
	</head>
	<body>
		{{ template "header" . }}
		<main>
			<div class="wrapper">
				{{ template "partials/recipe-edit" .Data }}
			</div>
		</main>
		{{ template "footer" . }}
	</body>
</html>
{{ end }}
{{ define "partials/recipe" }}
<article id="recipe-{{.ID}}" class="recipe">
	<section id="meta">
		<div class="recipe-header">
			<h1>{{ .Name }}</h1>
			<a href="{{.ID}}/edit" class="button textshadow nounderline">Edit</a>
		</div>
		<div class="recipe-image">
			<img class="responsive" src="{{ .Image }}" alt="Picture of {{ .Name }}"/>
		</div>
		{{ if .Description }}
			<p>
			{{ .Description }}
			</p>
		{{ end }}
		{{ if .PrepTime }}
		<p>
			<b>Prep time:</b>
			{{ .PrepTime }}
		</p>
		{{ end }}
		{{ if .CookTime }}
		<p>
			<b>Cook time:</b>
			{{ .CookTime }}
		</p>
		{{ end }}
		{{ if .Yields }}
		<p>
			Serves {{ .Yields }}
		</p>
		{{ end }}
	</section>
	<section id="ingredients">
		<h2>Ingredients</h2>
		<ul>
			{{ range $ingredient := .Ingredients }}
			<li>{{ $ingredient }}</li>
			{{ end }}
		</ul>
	</section>
	<section id="instructions">
		<h2>Instructions</h2>
		<ol>
			{{ range $instruction := .Instructions }}
			<li>{{ $instruction }}</li>
			{{ end }}
		</ol>
	</section>
</article>
{{ end }}

{{ define "recipe" }}
<!doctype html>
<html>
	<head>
		{{ template "head" . }}
	</head>
	<body>
		{{ template "header" . }}
		<main>
			<div class="wrapper">
				{{ template "partials/recipe" .Data }}
			</div>
		</main>
		{{ template "footer" . }}
	</body>
</html>
{{ end }}
{{ define "recipes" }}
<!doctype html>
<html>
	<head>
		{{ template "head" . }}
	</head>
	<body>
		{{ template "header" . }}
		<main>
			<div class="wrapper">
				<!--
				<nav class="flexbetween">
					<h2>Recipes</h2>
					<a href="/recipe-search">Advanced Search</a>
				</nav>
				-->
				<form class="searchform-mini">
					<input class="searchbar" name="query" placeholder="Search..." value="{{.Query.Get "query"}}"/>
				</form>
				<p class="result-count">{{ .Paginator.Count }} results</p>
				<div class="card-deck">
					{{ range $recipe := .Data }}
						{{ template "recipe-card" $recipe }}
					{{ end }}
				</div>
				{{ template "paginator" . }}
			</div>
		</main>
		{{ template "footer" . }}
	</body>
</html>
{{ end }}
`