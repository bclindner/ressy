package util

import (
	"sort"
	"fmt"
	"strings"
	"mime"
	"strconv"
	"net/http"
)

const DEFAULT_QUALITY = 10

type NoAcceptableFormatError struct{}

func (n NoAcceptableFormatError) Error() string {
	return "No acceptable format."
}

type ContentType struct {
	Mime string
	Params map[string]string
	Quality int
}

type contentTypes []ContentType

// sort.Interface
func (a contentTypes) Len() int { return len(a) }
func (a contentTypes) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a contentTypes) Less(i, j int) bool { return a[i].Quality < a[j].Quality }

func ContentNegotiate(r *http.Request, acceptableTypes []string) (ContentType, error) {
	var returnType ContentType
	types := contentTypes{}
	// parse content types from accept header
	for _, str := range strings.Split(r.Header.Get("Accept"), ",") {
		mtype, params, err := mime.ParseMediaType(str)
		if err != nil {
			return returnType, fmt.Errorf("Failed to parse mime from Accept header part '%s': %w", str, err)
		}
		if qstring, ok := params["q"]; ok {
			q, err := strconv.ParseFloat(qstring, 64)
			if err != nil {
				return returnType, fmt.Errorf("Failed to parse Q-value from Accept header part '%s': %w", str, err)
			}
			qint := int(q * 10)
			types = append(types, ContentType{
				Mime: mtype,
				Params: params,
				Quality: qint,
			})
		} else {
			types = append(types, ContentType{
				Mime: mtype,
				Params: params,
				Quality: DEFAULT_QUALITY,
			})
		}
	}
	// sort them
	sort.Sort(sort.Reverse(types))
	// iterate over the sorted-by-quality list, and find the first one that is in
	// the acceptableTypes slice
	for _, ctype := range types {
		// if they'll take anything, then just throw the best thing we got at 'em
		if ctype.Mime == "*/*" {
			return ContentType{
				Mime: acceptableTypes[0],
				Params: make(map[string]string),
				Quality: DEFAULT_QUALITY,
			}, nil
		}
		ctypearr := strings.Split(ctype.Mime, "/")
		for _, atype := range acceptableTypes {
			atypearr := strings.Split(atype, "/")
			// if exactly equal, or if the content type is <type>/*, then use that type
			if ctype.Mime == atype || (ctypearr[1] == "*" && ctypearr[0] == atypearr[0]) {
				return ctype, nil
			}
		}
	}
	// welp.
	return returnType, NoAcceptableFormatError{}
}
