package util

import (
	"net/url"
	"strconv"
)

// A struct representing an absolute pagination cursor.
type Paginator struct {
	// URL to base our NextURL and PrevURL from
	URL url.URL
	Count int
	Limit int
	Offset int
}

// Determine if the paginator can go back.
func (p *Paginator) Prev() bool {
	return p.Offset - p.Limit >= 0
}

// Determine if the paginator can go forward.
func (p *Paginator) Next() bool {
	return p.Offset < p.Count
}

// Get the offset for the previous page.
func (p *Paginator) PrevOffset() int {
	prevOffset := p.Offset - p.Limit
	if prevOffset < 0 {
		return 0
	}
	return prevOffset
}

// Get the offset for the next page.
func (p *Paginator) NextOffset() int {
	return p.Offset + p.Limit
}

// Get the URL for the previous page (if applicable)
// Check Prev() before using this
func (p Paginator) PrevURL() string {
	qp := p.URL.Query()
	qp.Set("offset", strconv.Itoa(p.PrevOffset()))
	p.URL.RawQuery = qp.Encode()
	return p.URL.String()
}

// Get the URL for the next page (if applicable)
// Check Next() before using this
func (p Paginator) NextURL() string {
	qp := p.URL.Query()
	qp.Set("offset", strconv.Itoa(p.NextOffset()))
	p.URL.RawQuery = qp.Encode()
	return p.URL.String()
}

// Get the current page number.
func (p *Paginator) CurrentPageNumber() int {
	if p.Offset == 0 {
		return 1
	} else {
		return p.Offset / p.Limit + 1
	}
}

// Get the maximum page number.
func (p *Paginator) MaxPageNumber() int {
	maxPageNum := p.Count / p.Limit
	if maxPageNum == 0 {
		return 1
	}
	return maxPageNum
}
