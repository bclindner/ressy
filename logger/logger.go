package logger

import (
	"log"
	"os"
	"io"
	"fmt"
)

type Logger struct {
	info *log.Logger
	warn *log.Logger
	err *log.Logger
	fatal *log.Logger
}

func NewLogger() *Logger {
	makeLogger := func(stream io.Writer, prefix string) *log.Logger {
		return log.New(stream, prefix, log.Ldate | log.Ltime | log.Lshortfile)
	}
	logger := Logger{}
	logger.info = makeLogger(os.Stdout, "[INFO] ")
	logger.warn = makeLogger(os.Stderr, "[WARN] ")
	logger.err = makeLogger(os.Stderr, "[ERROR]")
	logger.fatal = makeLogger(os.Stderr, "[FATAL] ")
	return &logger
}

func (l *Logger) InfoOutput(level int, format string, args ...interface{}) {
	l.info.Output(2 + level, fmt.Sprintf(format, args...))
}
func (l *Logger) Info(format string, args ...interface{}) {
	l.InfoOutput(1, format, args...)
}

func (l *Logger) WarnOutput(level int, format string, args ...interface{}) {
	l.warn.Output(2 + level, fmt.Sprintf(format, args...))
}
func (l *Logger) Warn(format string, args ...interface{}) {
	l.WarnOutput(1, format, args...)
}

func (l *Logger) ErrOutput(level int, format string, args ...interface{}) {
	l.err.Output(2 + level, fmt.Sprintf(format, args...))
}
func (l *Logger) Err(format string, args ...interface{}) {
	l.ErrOutput(1, format, args...)
}

func (l *Logger) FatalOutput(level int, format string, args ...interface{}) {
	l.fatal.Output(2 + level, fmt.Sprintf(format, args...))
	os.Exit(1)
}
func (l *Logger) Fatal(format string, args ...interface{}) {
	l.FatalOutput(1, format, args...)
}
