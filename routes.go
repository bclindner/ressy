package main

import (
	"net/http"
)

func (s *Server) declareBaseRoutes() {
	s.RouteFunc("GET", "/", s.GetHome, s.ContentNegotiationMiddleware([]string{"text/html"}))
}

func (s *Server) GetHome(w http.ResponseWriter, r *http.Request) {
	// serve home template
	s.serveTemplate(w, TemplateData{
		template: "home",
		Title: "Home",
		Data: nil,
	})
}
