//+build ignore

package main

import (
	"os"
	"io/ioutil"
	"text/template"
)


func main() {
	var templates string
	files, err := ioutil.ReadDir("templates")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		f, err := os.Open("templates/" + file.Name())
		if err != nil {
			panic(err)
		}
		defer f.Close()
		bytes, err := ioutil.ReadAll(f)
		if err != nil {
			panic(err)
		}
		templates += string(bytes)
	}
	outfile, err := os.Create("templates.go")
	if err != nil {
		panic(err)
	}
	err = tmpl.Execute(outfile, templates)
	if err != nil {
		panic(err)
	}
}

var tmpl = template.Must(template.New("").Parse(`
package main

// auto-generated template pack file

const TEMPLATES =` + "`" + `{{ . }}` + "`"))
