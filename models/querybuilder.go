package models

import (
	"strings"
	"database/sql"
)

// Ultra-primitive generic query builder.
// Really just made to make dynamic SELECT statements work.
type QueryBuilder struct {
	db *DB
	base string
	whereClauses []string
	whereVars []interface{}
	orderClauses []string
	orderVars []interface{}
	appendClauses []string
	appendVars []interface{}
}

func (d *DB) NewQueryBuilder() *QueryBuilder {
	return &QueryBuilder{
		db: d,
	}
}

func (q *QueryBuilder) SetBase(str string) *QueryBuilder {
	q.base = str
	return q
}

func (q *QueryBuilder) Where(str string, args ...interface{}) *QueryBuilder {
	q.whereClauses = append(q.whereClauses, str)
	if len(args) > 0 {
		q.whereVars = append(q.whereVars, args...)
	}
	return q
}

func (q *QueryBuilder) OrderBy(str string, args ...interface{}) *QueryBuilder {
	q.orderClauses = append(q.orderClauses, str)
	if len(args) > 0 {
		q.orderVars = append(q.orderVars, args...)
	}
	return q
}

func (q *QueryBuilder) Limit(num int) *QueryBuilder {
	q.appendClauses = append(q.appendClauses, "LIMIT ?")
	q.appendVars = append(q.appendVars, num)
	return q
}

func (q *QueryBuilder) Offset(num int) *QueryBuilder {
	q.appendClauses = append(q.appendClauses, "OFFSET ?")
	q.appendVars = append(q.appendVars, num)
	return q
}

func (q *QueryBuilder) generateSQL() (string, []interface{}) {
	sql := q.base
	var vars []interface{}
	if len(q.whereClauses) > 0 {
		sql = sql + " WHERE " + strings.Join(q.whereClauses, " AND ")
		vars = append(vars, q.whereVars...)
	}
	if len(q.orderClauses) > 0 {
		sql = sql + " ORDER BY " + strings.Join(q.orderClauses, ", ")
		vars = append(vars, q.orderVars...)
	}
	if len(q.appendClauses) > 0 {
		sql = sql + " " + strings.Join(q.appendClauses, " ")
		vars = append(vars, q.appendVars...)
	}
	return sql, vars
}

func (q *QueryBuilder) Query() (*sql.Rows, error) {
	sql, vars := q.generateSQL()
	return q.db.db.Query(sql, vars...)
}

func (q *QueryBuilder) QueryRow() *sql.Row {
	sql, vars := q.generateSQL()
	return q.db.db.QueryRow(sql, vars...)
}
