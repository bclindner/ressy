package models

import (
	"fmt"
	"time"
	"strings"
	"strconv"
)

const RECIPE_SQL_STRUCT = `
SELECT id,
name,
image,
description,
yields,
preptime,
cooktime
FROM recipes
`

const RECIPE_SQL_COUNT = `
SELECT count(*)
FROM recipes
`

// Both sql.Row and sql.Rows implement this, but there's nothing reasonably defined, so lemme uhhhh
type Scannable interface {
	Scan(...interface{}) error
}

// A semantic querybuilder for DB-independent recipe searching at service level.
type RecipeQueryBuilder struct {
	q *QueryBuilder
	d *DB
}

// Helper function for "fuzzy" searching by field.
func (r *RecipeQueryBuilder) fieldLike(name string, value interface{}) *RecipeQueryBuilder {
	r.q.Where(fmt.Sprintf("%s LIKE '%%' || ? || '%%'", name), value)
	return r
}

func (r *RecipeQueryBuilder) Since(id int) *RecipeQueryBuilder {
	r.q.Where("id > ?", id)
	return r
}

func (r *RecipeQueryBuilder) Limit(limit int) *RecipeQueryBuilder {
	r.q.Limit(limit)
	return r
}

func (r *RecipeQueryBuilder) Offset(offset int) *RecipeQueryBuilder {
	r.q.Offset(offset)
	return r
}

func (r *RecipeQueryBuilder) ByID(id int) *RecipeQueryBuilder {
	r.q.Where("id = ?", id)
	return r
}

func (r *RecipeQueryBuilder) ByName(name string) *RecipeQueryBuilder {
	r.fieldLike("name", name)
	return r
}

func (r *RecipeQueryBuilder) ByDescription(desc string) *RecipeQueryBuilder {
	r.fieldLike("description", desc)
	return r
}

func (r *RecipeQueryBuilder) ByYields(yields string) *RecipeQueryBuilder {
	r.fieldLike("yields", yields)
	return r
}

func (r *RecipeQueryBuilder) ByPrepTime(prepTime time.Duration) *RecipeQueryBuilder {
	r.q.Where("prepTime < ?", prepTime)
	return r
}

func (r *RecipeQueryBuilder) ByCookTime(cookTime time.Duration) *RecipeQueryBuilder {
	r.q.Where("cookTime < ?", cookTime)
	return r
}

func (r *RecipeQueryBuilder) ByIngredient(ingredient string) *RecipeQueryBuilder {
	r.q.Where("id in (SELECT recipe_id FROM recipe_ingredients WHERE name LIKE '%' || ? || '%')", ingredient)
	return r
}

func (r *RecipeQueryBuilder) ByInstruction(instruction string) *RecipeQueryBuilder {
	r.q.Where("id in (SELECT recipe_id FROM recipe_instructions WHERE name LIKE '%' || ? || '%')", instruction)
	return r
}

func (r *RecipeQueryBuilder) ByQuery(query string) *RecipeQueryBuilder {
	terms := strings.Split(query, " ")
	for _, term := range terms {
		tuple := strings.Split(term, ":")
		if len(tuple) == 1 {
			r.ByName(tuple[0])
		} else {
			namespace := tuple[0]
			var value string
			if len(tuple) > 2 {
				value = strings.Join(tuple[1:],":")
			} else {
				value = tuple[1]
			}
			value = strings.Replace(value, "_", " ", 0)
			switch namespace {
			case "id":
				intvalue, err := strconv.Atoi(value)
				if err != nil {
					r.q.Where("id = NULL") // that's on them lol
				}
				r.ByID(intvalue)
			case "ingredient":
				fallthrough
			case "ing":
				r.ByIngredient(value)
			case "instruction":
				fallthrough
			case "ins":
				r.ByInstruction(value)
			case "description":
				fallthrough
			case "desc":
				r.ByDescription(value)
			case "yields":
				fallthrough
			case "yield":
				r.ByYields(value)
			case "name":
				fallthrough
			default:
				r.ByName(value)
			}
		}
	}
	return r
}
// Pull a recipe from a row.
// The way this is set up depends on using the query layout from db.GetRecipes().
func (r *RecipeQueryBuilder) scanRecipe(scanner Scannable) (*Recipe, error) {
		recipe := &Recipe{}
		err := scanner.Scan(&recipe.ID, &recipe.Name, &recipe.Image, &recipe.Description, &recipe.Yields, &recipe.PrepTime, &recipe.CookTime)
		if err != nil {
			return recipe, fmt.Errorf("Failed to scan recipe: %w", err)
		}
		// get ingredients
		rows, err := r.d.db.Query("SELECT name FROM recipe_ingredients WHERE recipe_id = ?", recipe.ID)
		if err != nil {
			return recipe, fmt.Errorf("Failed to run ingredients SELECT: %w", err)
		}
		for rows.Next() {
			var ingredient Ingredient
			if err := rows.Scan(&ingredient); err != nil {
				return recipe, fmt.Errorf("Failed to scan ingredients: %w", err)
			}
			recipe.Ingredients = append(recipe.Ingredients, ingredient)
		}
		// get instructions
		rows, err = r.d.db.Query("SELECT name FROM recipe_instructions WHERE recipe_id = ? ORDER BY step", recipe.ID)
		if err != nil {
			return recipe, fmt.Errorf("Failed to run instructions SELECT: %w", err)
		}
		for rows.Next() {
			var instruction Instruction
			if err := rows.Scan(&instruction); err != nil {
				return recipe, fmt.Errorf("Failed to scan instructions: %w", err)
			}
			recipe.Instructions = append(recipe.Instructions, instruction)
		}
		return recipe, nil
}

// Get all rows from query.
func (r *RecipeQueryBuilder) GetAll() ([]*Recipe, error) {
	rs := []*Recipe{}
	r.q.SetBase(RECIPE_SQL_STRUCT)
	rows, err := r.q.Query()
	if err != nil {
		return rs, fmt.Errorf("Failed to run recipe SELECT: %w", err)
	}
	for rows.Next() {
		recipe, err := r.scanRecipe(rows)
		if err != nil {
			return rs, fmt.Errorf("Failed to scan recipe: %w", err)
		}
		rs = append(rs, recipe)
	}
	return rs, nil
}

// Get a single row from the query.
func (r *RecipeQueryBuilder) Get() (*Recipe, error) {
	r.q.SetBase(RECIPE_SQL_STRUCT)
	row := r.q.QueryRow()
	recipe, err := r.scanRecipe(row)
	if err != nil {
		return recipe, fmt.Errorf("Failed to scan recipe: %w", err)
	}
	return recipe, nil
}

// Get the number of rows returned from the query.
func (r *RecipeQueryBuilder) GetCount() (int, error) {
	r.q.SetBase(RECIPE_SQL_COUNT)
	row := r.q.QueryRow()
	var count int
	err := row.Scan(&count)
	if err != nil {
		return count, fmt.Errorf("Failed to scan count: %w", err)
	}
	return count, nil
}
