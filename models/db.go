package models

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"git.local/bclindner/ressy/logger"
	"fmt"
)

type DB struct {
	db *sql.DB
	logger *logger.Logger
}

func NewDB(logger *logger.Logger) (db *DB, err error) {
	// set up DB
	sqldb, err := sql.Open("sqlite3", "./ressy.db")
	if err != nil {
		return db, fmt.Errorf("Failed to open DB: %w", err)
	}
	// ping to make sure we're connected
	err = sqldb.Ping()
	if err != nil {
		return db, fmt.Errorf("Failed to ping DB: %w", err)
	}
	db = &DB{
		db: sqldb,
		logger: logger,
	}
	// handle migrations, if necessary
	err = db.handleMigrations()
	if err != nil {
		return db, fmt.Errorf("Failed to migrate: %w", err)
	}
	return db, nil
}
