package models

import (
	"database/sql"
)

const (
	DBVERSION = 1
)

func (d *DB) getDBVersion() (version int, err error) {
	row := d.db.QueryRow("SELECT * FROM sqlite_master WHERE type= 'table' and name='meta'")
	err = row.Scan(&version)
	if err != nil {
		// if it just can't find it, that means we probably have an uninitialized
		// database
		if err == sql.ErrNoRows {
			return 0, nil
		} else {
			return version, err
		}
	}
	row = d.db.QueryRow("SELECT * FROM meta WHERE name = ?", "db_version")
	err = row.Scan(&version)
	if err != nil {
		return version, err
	}
	return version, nil
}

func (d *DB) handleMigrations() error {
	version, err := d.getDBVersion()
	if err != nil {
		return nil
	}
	if version < DBVERSION {
		d.logger.Info("Looks like DB needs some migration (at version %d, need version %d)...", version, DBVERSION)
		return d.migrate1(version)
	} else if version > DBVERSION {
		d.logger.Fatal("DB version is higher than the current version - cannot continue with this DB!")
	}
	return nil
}

// Initial migration schema.
func (d *DB) migrate1(version int) error {
	d.logger.Info("Starting migration to version 1")
	tx, err := d.db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Exec(`
		DROP TABLE IF EXISTS recipes;
		CREATE TABLE recipes (
			id integer PRIMARY KEY,
			name text,
			image text,
			description text,
			yields text,
			preptime integer,
			cooktime integer,
			deleted_at text
		);
		DROP TABLE IF EXISTS recipe_ingredients;
		CREATE TABLE recipe_ingredients (
			recipe_id integer,
			ingredient_order integer,
			name text,
			PRIMARY KEY (recipe_id, ingredient_order),
			FOREIGN KEY (recipe_id) REFERENCES recipes(id)
		);
		DROP TABLE IF EXISTS recipe_instructions;
		CREATE TABLE recipe_instructions (
			recipe_id integer,
			step integer,
			name text,
			PRIMARY KEY (recipe_id, step),
			FOREIGN KEY (recipe_id) REFERENCES recipes(id)
		);
		-- name-value table for db metadata and some global settings
		DROP TABLE IF EXISTS meta;
		CREATE TABLE meta (
			name text PRIMARY KEY,
			value text
		);
		-- insert first DB version (1)
		INSERT INTO meta (name, value) VALUES ('db_version', '1');
	`)
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	d.logger.Info("Migrated DB to version 1")
	return nil
}
