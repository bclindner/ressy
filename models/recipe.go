package models

import (
	"time"
	"fmt"
)

// A incomplete version of the schema.org/Recipe schema.
type Recipe struct {
	// ID of the recipe in the DB.
	ID int64 `json:"id"`
	// The name of the food/recipe being made.
	Name string `json:"name"`
	// URL to a cover image for the recipe.
	Image string `json:"image"`
	// Description of the recipe - generally 1 or 2 sentences.
	Description string `json:"description"`
	// How many services the recipe yields.
	Yields string `json:"yields"`
	// Time it takes to prep the recipe.
	PrepTime time.Duration `json:"prepTime"`
	// Time it takes to cook the recipe.
	CookTime time.Duration `json:"cookTime"`
	// List of ingredients.
	Ingredients []Ingredient `json:"ingredients"`
	// List of instructions.
	Instructions []Instruction `json:"instructions"`
}

// Ingredient type.
type Ingredient string

// Instruction type.
type Instruction string

// Start a new recipe query.
// See recipe_querybuilder.go for details on how this works.
func (d *DB) GetRecipes() *RecipeQueryBuilder {
	qb := d.NewQueryBuilder()
	// only get undeleted recipes!
	qb.Where("deleted_at IS NULL")
	qb.OrderBy("id DESC")
	return &RecipeQueryBuilder{
		q: qb,
		d: d,
	}
}

// Insert a new recipe into the DB.
// Ignores any set ID on the recipe and simply INSERTs it into the DB.
// Returns the ID of the created recipe.
func (d *DB) InsertRecipe(r Recipe) (id int64, err error) {
	tx, err := d.db.Begin()
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("Could not start transaction: %w", err)
	}
	res, err := tx.Exec(`INSERT INTO recipes (
		name,
		image,
		description,
		yields,
		preptime,
		cooktime
	) VALUES (?, ?, ?, ?, ?, ?)`, r.Name, r.Image, r.Description, r.Yields, r.PrepTime, r.CookTime)
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("Could not INSERT recipe: %w", err)
	}
	// we need the ID of the recipe inserted to insert the related data
	id, err = res.LastInsertId()
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("Could not get last inserted ID: %w", err)
	}
	// insert ingredients
	for order, ingredient := range r.Ingredients {
		_, err := tx.Exec("INSERT INTO recipe_ingredients (recipe_id, ingredient_order, name) values (?, ?, ?)", id, order, ingredient)
		if err != nil {
			tx.Rollback()
			return id, fmt.Errorf("Could not INSERT recipe ingredient: %w", err)
		}
	}
	// insert instructions
	for step, instruction := range r.Instructions {
		_, err := tx.Exec("INSERT INTO recipe_instructions (recipe_id, step, name) values (?, ?, ?)", id, step, instruction)
		if err != nil {
			tx.Rollback()
			return id, fmt.Errorf("Could not INSERT recipe ingredient: %w", err)
		}
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return id, fmt.Errorf("Could not commit recipe insert transaction: %w", err)
	}
	return id, nil
}

// Update a recipe with the new given object.
// The passed object must be authoritative.
// TODO Improve this - there is a LOT of extra crap being updated here that
// could probably be skipped if we had some way of transmitting a *partial*
// recipe object.
func (d *DB) UpdateRecipe(r Recipe) error {
	tx, err := d.db.Begin()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not start transaction: %w", err)
	}
	_, err = tx.Exec(`UPDATE recipes SET
		name = ?,
		image = ?,
		description = ?,
		yields = ?,
		preptime = ?,
		cooktime = ?
		WHERE id = ?
		AND deleted_at IS NULL`, r.Name, r.Image, r.Description, r.Yields, r.PrepTime, r.CookTime, r.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not UPDATE recipe: %w", err)
	}
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not get last inserted ID: %w", err)
	}
	// insert ingredients
	var maxOrder int
	for order, ingredient := range r.Ingredients {
		_, err := tx.Exec("INSERT OR REPLACE INTO recipe_ingredients (recipe_id, ingredient_order, name) values (?, ?, ?)", r.ID, order, ingredient)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("Could not INSERT/REPLACE recipe ingredient: %w", err)
		}
		maxOrder = order
	}
	// delete any out-of-range ingredients
	_, err = tx.Exec("DELETE FROM recipe_ingredients WHERE recipe_id = ? AND ingredient_order > ?", r.ID, maxOrder)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not delete out-of-range ingredients: %w", err)
	}
	// insert instructions
	var maxStep int
	for step, instruction := range r.Instructions {
		_, err := tx.Exec("INSERT OR REPLACE INTO recipe_instructions (recipe_id, step, name) values (?, ?, ?)", r.ID, step, instruction)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("Could not INSERT/REPLACE recipe instruction: %w", err)
		}
		maxStep = step
	}
	// delete any out-of-range instructions
	_, err = tx.Exec("DELETE FROM recipe_instructions WHERE recipe_id = ? AND step > ?", r.ID, maxStep)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not delete out-of-range ingredients: %w", err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not commit recipe insert transaction: %w", err)
	}
	return nil
}

// Delete a recipe with the given integer.
func (d *DB) DeleteRecipe(id int) error {
	tx, err := d.db.Begin()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not start transaction: %w", err)
	}
	_, err = tx.Exec("UPDATE recipes SET deleted_at = datetime('now') WHERE id = ?", id)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not update deleted_at for recipe: %w", err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("Could not commit recipe insert transaction: %w", err)
	}
	return nil
}
