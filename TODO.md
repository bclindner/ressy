# TODO

## Bugs

* If the number of steps is reduced when running updateRecipe, the method needs
	to delete the old steps
* handleError should return a correctly-formatted error based on Accept header
* Paginator buttons currently fullwidth due to button changes

## HTML/UX

## Backend/API

* Query param filtering for cook and prep time?
* PUT request for /recipes/:id
* Import and save recipe images locally

# Backlog

* Schema.org/Recipe (`application/ld+json`) -> models.Recipe support
	* May only be partial at first; schema is kinda big
	* May require enhancements to existing data
	* Will likely require github.com/piprate/json-gold for flatten
* Image recipe steps?
* Remove httprouter?
	* ServeMux is trash though
