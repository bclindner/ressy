package main

import (
	"net/http"
	"io/ioutil"
)

const PORT = ":8080"

func main() {
	server := NewServer()
	// runImportJob(server)
	server.logger.Info("listening on %s", PORT)
	err := http.ListenAndServe(PORT, server)
	if err != nil {
		server.logger.Fatal("Server closed: %s", err)
	}
}

func runImportJob(server *Server) {
	files, err := ioutil.ReadDir("recipes")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		recipe, err := importRecipe("recipes/" + file.Name())
		if err != nil {
			panic(err)
		}
		_, err = server.db.InsertRecipe(*recipe)
		if err != nil {
			panic(err)
		}
	}
}
