package main

import (
	"github.com/julienschmidt/httprouter"
	"net/url"
	"net/http"
	"html/template"
	"encoding/json"

	"git.local/bclindner/ressy/logger"
	"git.local/bclindner/ressy/models"
	"git.local/bclindner/ressy/util"
)

//go:generate go run scripts/pack_templates.go

// TemplatePage represents data passed to a template.
type TemplateData struct {
	// The name of the template to render.
	template string
	// The page title.
	Title string
	// The query params of the URL.
	// Used to make the query value sticky, among other things.
	Query url.Values
	// Paginator. Used in lists.
	Paginator *util.Paginator
	// The data to be used by the template proper.
	Data interface{}
}

// Server is the space that holds global variables for routes and general server operations.
type Server struct {
	// The database object.
	//
	// Functions using this should not call the underlying database, but rather
	// use the exposed service functions to perform DB actions. Rather than
	// interfacing directly, a new method should be added to the models.DB struct.
	db *models.DB
	// The logger object, usable by anything that uses Server to log info, errors,
	// etc.
	logger *logger.Logger
	// The router, which the below ServeHTTP function uses to serve content.
	router *httprouter.Router
	// The template renderer.
	//
	// Generally only used by the router, and initialized at runtime. Do not
	// change this at any point outside of NewServer().
	templates *template.Template
}

// Create a new server.
// Logs a fatal error and closes if it cannot be created.
func NewServer() *Server {
	// set up logger
	logger := logger.NewLogger()
	// build templates
	templates, err := template.New("").Parse(TEMPLATES)
	if err != nil {
		logger.Fatal("Failed to parse templates: %s", err)
	}
	// set up DB
	db, err := models.NewDB(logger)
	if err != nil {
		logger.Fatal("Failed to open DB connection: %s", err)
	}
	// set up router
	router := httprouter.New()
	// set up server object (we must do this before the routes)
	srv := Server{
		db: db,
		logger: logger,
		router: router,
		templates: templates,
	}
	router.ServeFiles("/assets/*filepath", http.Dir("assets/"))
	srv.declareRecipeRoutes()
	srv.declareBaseRoutes()
	return &srv
}

func (s *Server) RouteFunc(method, route string, handler http.HandlerFunc, middleware ...Middleware) {
	s.Route(method,route,http.HandlerFunc(handler), middleware...)
}

func (s *Server) Route(method, route string, handler http.Handler, middleware ...Middleware) {
	// common middleware
	middleware = append(middleware, s.LogMiddleware)
	// handle
	s.router.Handler(method, route, ChainMiddleware(
		handler,
		middleware...
	))
}

// A wrapper around the internal httprouter which allows the server itself to
// act directly as an http.Handler.
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

// Serve a struct as JSON using a json.Encoder.
//
// This function handles its own errors with the handleError function, and
// should generally be the last function called in a route, as it writes the
// final output to the body.
func (s *Server) serveJSON(w http.ResponseWriter, d interface{}) {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(&d)
	if err != nil {
		s.handleError(w, 500, err)
	}
}

// Serve an HTML template from the Server's template directory.
//
// This function handles its own errors with the handleError function, and
// should generally be the last function called in a route, as it writes the
// final output to the body.
func (s *Server) serveTemplate(w http.ResponseWriter, p TemplateData) {
	w.Header().Set("Content-Type", "text/html")
	err := s.templates.ExecuteTemplate(w, p.template, p)
	if err != nil {
		s.handleError(w, 500, err)
	}
}

// Handle an HTTP error.
//
// TODO: Implement proper error rendering.
// http.Error sends errors as plaintext, which really irks me given this site is
// designed for JSON and HTML.
func (s *Server) handleError(w http.ResponseWriter, code int, err error) {
	http.Error(w, http.StatusText(code), code)
	if err != nil {
		s.logger.ErrOutput(1, "%s", err)
	}
}
