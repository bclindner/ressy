package main

import (
	"git.local/bclindner/ressy/models"
	"os"
	"encoding/json"
	"fmt"
)

type schemaorgRecipe struct {
	Name string `json:"name"`
	Description string `json:"description"`
	Image []string `json:"image"`
	RecipeYield string `json:"recipeYield"`
	RecipeIngredient []string `json:"recipeIngredient"`
	RecipeInstructions []schemaorgHowToStep `json:"recipeInstructions"`
}
type schemaorgHowToStep struct {
	Text string `json:"text"`
}

func importRecipe(filename string) (recipe *models.Recipe, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return recipe, fmt.Errorf("Error opening file: %w",err)
	}
	var importedRecipe schemaorgRecipe
	err = json.NewDecoder(file).Decode(&importedRecipe)
	if err != nil {
		return recipe, fmt.Errorf("Error decoding file: %w",err)
	}
	var ingredients []models.Ingredient
	for _, ingredient := range importedRecipe.RecipeIngredient {
		ingredients = append(ingredients, models.Ingredient(ingredient))
	}
	var instructions []models.Instruction
	for _, instruction := range importedRecipe.RecipeInstructions {
		instructions = append(instructions, models.Instruction(instruction.Text))
	}
	recipe = &models.Recipe{
		Name: importedRecipe.Name,
		Description: importedRecipe.Description,
		Image: importedRecipe.Image[0],
		Yields: importedRecipe.RecipeYield,
		Ingredients: ingredients,
		Instructions: instructions,
	}
	return recipe, nil
}
